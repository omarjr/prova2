
package prova2;

public class ConsultorAgenciaGoverno extends Usuario{
    /*Essa classe foi criada para realizar a função de uma pessoa encarregada de
     ver os posts e comentários criados no site para passar à agência a qual ele
     é funcionário. Dessa forma, haveria a ligacao indireta entre o cidadão e a
     agência a qual ele está criticando.*/
    
    public void verPost(Post post) {
        /**/
    }
    
    public void verComentarios(Comentario comentario) {
        /**/
    }
}
