package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import prova2.Cidadao;

public class TestCidadao {
	Cidadao umCidadao;

	@Before
	public void setUp() throws Exception {
		umCidadao = new Cidadao();
	}

	@Test
	public void testNome() {
		umCidadao.setNome("Fulano");
		assertEquals("Fulano", umCidadao.getNome());
	}
	
	@Test
	public void testEmail() {
		umCidadao.setEmail("fulano@fulano.com");
		assertEquals("fulano@fulano.com", umCidadao.getEmail());
	}
	
	@Test
	public void testSenha() {
		umCidadao.setSenha("fulanogatinho");
		assertEquals("fulanogatinho", umCidadao.getSenha());
	}
	/*Isso mostra que a subClasse Cidadão realmente herda os atributos e métodos do super Usuário!!*/
	
}
