package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import prova2.Endereco;

public class TestEndereco {
	Endereco umEndereco;
	
	@Before
	public void setUp() throws Exception {
		umEndereco = new Endereco();
	}

	@Test
	public void testPais() {
		umEndereco.setPais("Tão Tão Distante");
		assertEquals("Tão Tão Distante", umEndereco.getPais());
	}
	
	@Test
	public void testEstado() {
		umEndereco.setEstado("Bem Longe");
		assertEquals("Bem Longe", umEndereco.getEstado());
	}
	
	@Test
	public void testCidade() {
		umEndereco.setCidade("Aqui Perto");
		assertEquals("Aqui Perto", umEndereco.getCidade());
	}

}
