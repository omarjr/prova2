package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import prova2.Usuario;

public class TestUsuario {
	Usuario umUsuario;

	@Before
	public void setUp() throws Exception {
		umUsuario=new Usuario();
	}

	@Test
	public void testNome() {
		umUsuario.setNome("Fulano");
		assertEquals("Fulano", umUsuario.getNome());
	}
	
	@Test
	public void testEmail() {
		umUsuario.setEmail("fulano@fulano.com");
		assertEquals("fulano@fulano.com", umUsuario.getEmail());
	}
	
	@Test
	public void testSenha() {
		umUsuario.setSenha("fulanogatinho");
		assertEquals("fulanogatinho", umUsuario.getSenha());
	}
	
	

}
