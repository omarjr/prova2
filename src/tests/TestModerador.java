package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import prova2.Moderador;

public class TestModerador {
	Moderador umModerador;
	
	@Before
	public void setUp() throws Exception {
		umModerador = new Moderador();
	}

	@Test
	public void testNome() {
		umModerador.setNome("Fulano");
		assertEquals("Fulano", umModerador.getNome());
	}
	
	@Test
	public void testEmail() {
		umModerador.setEmail("fulano@fulano.com");
		assertEquals("fulano@fulano.com", umModerador.getEmail());
	}
	
	@Test
	public void testSenha() {
		umModerador.setSenha("fulanogatinho");
		assertEquals("fulanogatinho", umModerador.getSenha());
	}
	/*Isso mostra que a subClasse Moderador também herda os atributos e métodos do super Usuário!!*/

}
