package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import prova2.Participacao;

public class TestParticipacao {
	Participacao umaParticipacao;

	@Before
	public void setUp() throws Exception {
		umaParticipacao = new Participacao(null);
	}

	@Test
	public void testTitulo() {
		umaParticipacao.setTitulo("Não haverá Copa!!");
		assertEquals("Não haverá Copa!!", umaParticipacao.getTitulo());
	}
	
}
