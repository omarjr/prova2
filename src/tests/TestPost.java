package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import prova2.Post;

public class TestPost {
	Post umPost;

	@Before
	public void setUp() throws Exception {
		umPost = new Post(null, null);
	}

	@Test
	public void testTitulo() {
		umPost.setTitulo("Não haverá Copa!!");
		assertEquals("Não haverá Copa!!", umPost.getTitulo());
	}
	
	@Test
	public void testDescricao() {
		umPost.setDescricao("Blablabla");
		assertEquals("Blablabla", umPost.getDescricao());
	}
	
	@Test
	public void testObjetivo() {
		umPost.setObjetivo("Blablabla");
		assertEquals("Blablabla", umPost.getObjetivo());
	}

}
